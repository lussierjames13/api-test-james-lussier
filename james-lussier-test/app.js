const fs = require('fs');
const axios = require('axios')
const argv = require('yargs')
    .options({
        u: {
            demand: false,
            alias: 'username',
            default: 'calanceus',
            describe: 'Username for repository',
            string: true
        },
        r: {
            demand: false,
            alias: "repo",
            default: 'api-test',
            describe: "Repository name",
            string: true
        }
    })
    .help()
    .alias('help', 'h')
    .argv;

var url = `https://api.bitbucket.org/2.0/repositories/${argv.u}/${argv.r}/commits`
var file = "messages.txt"

axios.get(url).then((response) => {
    var data = []
    // Parse through all the values in reverse order of the response object
    response.data.values.reverse().forEach(element => {
        // Add each of their raw message values into an array for writing to a file
        data.push(element.rendered.message.raw)
    });
    // Write commit messages to file, join array to ignore commas
    fs.writeFile(file, data.join(""), (err) => {
        if (err) throw err;
        console.log(`Messages have been saved to file: ${file}`);
    })
}).catch((e) => {
    // If there is an error, handle it based upon the status code
    if(e.response.status == 403) {
        console.log(`The chosen repository(${argv.r}) is private. Access Denied.`)
    } else if (e.response.status == 404) {
        console.log(`The chosen repository (${argv.r}) or user (${argv.u}) does not exist.`)
    }
})